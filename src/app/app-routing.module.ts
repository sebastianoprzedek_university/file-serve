import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FilesComponent} from './components/files/files.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {RegulationsComponent} from "./components/regulations/regulations.component";
import {SharedListComponent} from "./components/shared-list/shared-list.component";
import {FriendsComponent} from "./components/friends/friends.component";
import {SharedResourceComponent} from "./components/shared-resource/shared-resource.component";
import {MySharingsComponent} from "./components/my-sharings/my-sharings.component";

const routes: Routes = [
  {path: 'files', children: [{path: '**', component: FilesComponent}]},
  {path: 'friends', component: FriendsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'my-sharings', component: MySharingsComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'regulations', component: RegulationsComponent},
  {path: 'shared', component: SharedListComponent},
  {path: 'share', children: [{path: '**', component: SharedResourceComponent}]},
  {path: '', redirectTo: '/files', pathMatch: 'full'},
  {path: '**', redirectTo: '/files', pathMatch: 'full'}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
