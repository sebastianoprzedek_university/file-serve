import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {APIService} from "./services/api.service";
import {StorageService} from "./services/storage.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private router: Router,
    private apiService: APIService,
    private storageService: StorageService) {
  }

  public logout() {
    this.apiService.logout().subscribe(
      data => {
        this.storageService.clear();
        this.router.navigate(['/login']);
        },
      err => console.error(err)
    );
  }

  authenticated(): boolean {
    return this.storageService.isAuthenticated();
  }

  username(): string {
    const username = this.storageService.getUsername();
    return username == null ? '' : username;
  }
}
