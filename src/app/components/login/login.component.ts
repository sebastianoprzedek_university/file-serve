import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {APIService} from "../../services/api.service";
import {StorageService} from "../../services/storage.service";

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public credentials = {username: '', password: ''};
  public error = false;

  constructor(
      private apiService: APIService, private router: Router, private storageService: StorageService) {
  }

  ngOnInit() {
    this.apiService.logout().subscribe(
      data => {
        this.storageService.clear();
        console.log("logged out");
      },
      err => console.error(err)
    );
  }

  loginForm = new FormGroup ({
    username: new FormControl(),
    password: new FormControl()
  });

  login(event: Event) {
    event.preventDefault();
    this.credentials.username = this.loginForm.controls['username'].value;
    this.credentials.password = this.loginForm.controls['password'].value;
    this.error = false;
    this.apiService.login(this.credentials).subscribe(data => {
        this.storageService.login();
        this.storageService.setUsername(this.loginForm.controls['username'].value);
        this.router.navigate(['/files']);
      },
      err => {
        console.log(err);
        this.error = true;
      }
    );
  }
 }
