import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {Router} from '@angular/router';
import {File} from '../../models/file.model'
import {StorageService} from "../../services/storage.service";
import {User} from "../../models/user.model";

@Component({
  templateUrl: './shared-list.component.html',
  styleUrls: ['./shared-list.component.css']
})
export class SharedListComponent implements OnInit {

  public sharedFiles = new Array<File>();
  public users = new Array<User>();

  constructor(
    private apiService: APIService,
    private storageService: StorageService,
    private router: Router) {
  }

  ngOnInit() {
    this.getSharedFiles();
    this.getUsers();
  }

  public getSharedFiles() {
    if (!this.storageService.isAuthenticated()) {
      this.router.navigate(['/login']);
    }
    this.apiService.getSharedFiles().subscribe(
      data => this.sharedFiles = data,
      err => this.router.navigate(['/login']));
  }

  public goToDirectory(file) {
    this.router.navigate(['/share/' + file.id])
  }

  public getUsers() {
    this.apiService.getUsers().subscribe(
      data => this.users = data,
      err => console.error(err)
    );
  }

  public findUsernameById(id) {
    if (this.users.filter(friend => friend.id == id).length > 0)
      return this.users.filter(friend => friend.id == id).pop().username;
    else
      return "";
  }

  public download(file) {
    this.apiService.downloadSharedResource('/' + file.id)
  }

  public filesNotEmpty() {
    return this.sharedFiles.length !== 0;
  }

  public getFileAsset(filename) {
    let extension = filename.split('.').pop();
    if (extension == "gitignore") extension = "git";
    else if (extension == "gitkeep") extension = "git";
    else if (extension == "docx") extension = "doc";
    else if (extension == "xlsx") extension = "xls";
    let list = ["zip", "xml", "xls", "txt", "svg", "sql", "rar", "png", "php", "pdf", "mp3", "md", "json", "js", "jpg", "ini", "html", "gz", "git", "gif", "exe", "doc", "dll", "default", "csv", "css", "bmp", "py"];
    if (list.includes(extension))
      return "assets/img/" + extension + ".png";
    else
      return "assets/img/default.png";
  }
}
