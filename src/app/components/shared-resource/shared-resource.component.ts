 import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {ActivatedRoute, Router, UrlSegment} from '@angular/router';
import {File} from '../../models/file.model'
import {StorageService} from "../../services/storage.service";

@Component({
  templateUrl: './shared-resource.component.html',
  styleUrls: ['./shared-resource.component.css']
})
export class SharedResourceComponent implements OnInit {

  public files = new Array<File>();
  public segments = new Array<UrlSegment>();

  constructor(
    private apiService: APIService,
    private storageService: StorageService,
    private router: Router,
    aroute: ActivatedRoute) {
    aroute.url.subscribe((data) => {
      this.segments = data;
      console.log(this.router.url);
    });
  }

  ngOnInit() {
    this.getSharedFiles();
  }

  public getSharedFiles() {
    this.apiService.getSharedResource(this.router.url).subscribe(
      data => {
        try {
          this.files = JSON.parse(data);
        } catch(e) {
          this.apiService.downloadSharedResource("/" + this.segments[0]);
          this.router.navigate(["/files"]);
        }
      },
      err => this.router.navigate(['/login']));
  }

  public goToLowerDirectory(file) {
    if (file.is_dir) {
      this.router.navigate(['share/' + this.segments[0] + "/" + file.path]).then(() => {
        this.getSharedFiles();
      });
    }
  }

  public goToUpperDirectory(segmentIndex) {
    console.log(segmentIndex);
    let path = "";
    for (let i = 0; i <= segmentIndex; i++) {
      path += this.segments[i].path + "/"
    }
    console.log(this.segments);
    console.log("PATH:" + path);
    this.router.navigate(['share/'+path]).then(()=>{
      this.getSharedFiles();
    });
  }

  public download(file) {
    this.apiService.downloadSharedResource("/" + this.segments[0] + file.path)
  }

  public filesNotEmpty() {
    return this.files.length !== 0;
  }

  public getFileAsset(filename) {
    let extension = filename.split('.').pop();
    if (extension == "gitignore") extension = "git";
    else if (extension == "gitkeep") extension = "git";
    else if (extension == "docx") extension = "doc";
    else if (extension == "xlsx") extension = "xls";
    let list = ["zip", "xml", "xls", "txt", "svg", "sql", "rar", "png", "php", "pdf", "mp3", "md", "json", "js", "jpg", "ini", "html", "gz", "git", "gif", "exe", "doc", "dll", "default", "csv", "css", "bmp", "py"];
    if (list.includes(extension))
      return "assets/img/" + extension + ".png";
    else
      return "assets/img/default.png";
  }
}
