import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {Router} from '@angular/router';
import {StorageService} from "../../services/storage.service";
import {SharedFile} from "../../models/shared-file.model";
import {User} from "../../models/user.model";

@Component({
  templateUrl: './my-sharings.component.html',
  styleUrls: ['./my-sharings.component.css']
})
export class MySharingsComponent implements OnInit {

  public sharedFiles = new Array<SharedFile>();
  public users = new Array<User>();
  public friendNames = new Array<string>();
  public showDeleteSharingSuccessMessage = false;
  public showDeleteSharingErrorMessage = false;
  public showGetSharingInfoErrorMessage = false;

  constructor(
    private apiService: APIService,
    private storageService: StorageService,
    private router: Router) {
  }

  ngOnInit() {
    this.getSharedFiles();
  }

  public getSharedFiles() {
    this.apiService.getSharedByMeFiles().subscribe(
      data => {
        this.sharedFiles = data;
        console.log(this.sharedFiles);
      },
        err => this.router.navigate(['/login'])
      );
  }

  public refreshMessages() {
    this.showDeleteSharingSuccessMessage = false;
    this.showDeleteSharingErrorMessage = false;
    this.showGetSharingInfoErrorMessage = false;
  }

  public deleteSharing(sharedFile) {
    this.refreshMessages();
    this.apiService.unshare(sharedFile.id).subscribe(
      data => {
        this.showDeleteSharingSuccessMessage = true;
        this.getSharedFiles();
      },
        err => this.showDeleteSharingErrorMessage = true
    )
  }

  public showFriends(sharedFile) {
    this.refreshMessages();
    this.apiService.getUsers().subscribe(
      data => {
        let friendNames = new Array<string>();
        sharedFile.shared_with.forEach(function (friendId) {
          friendNames.push(data.filter(user => user.id == friendId).map(user => user.username).pop());
        });
        this.friendNames = friendNames;
      },
      err => this.showDeleteSharingErrorMessage = true
    );
    document.getElementById('showShowFriendsModalButton').click();
  }

  public filesNotEmpty() {
    return this.sharedFiles.length !== 0;
  }
}
