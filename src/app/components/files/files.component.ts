import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {ActivatedRoute, Router, UrlSegment} from '@angular/router';
import {File} from '../../models/file.model'
import {StorageService} from "../../services/storage.service";
import {SharedFile} from "../../models/shared-file.model";
import {User} from "../../models/user.model";
import {Stat} from "../../models/stat.model";

@Component({
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {

  public files = new Array<File>();
  public segments = new Array<UrlSegment>();
  public stats = new Array<Stat>();
  public showDeleteErrorMessage = false;
  public showAddFileErrorMessage = false;
  public showAddDirErrorMessage = false;
  public showInvalidFolderNameMessage = false;
  public showEmptyFileMessage = false;
  public showShareErrorMessage = false;
  public showShareSuccessMessage = false;
  public showEncryptErrorMessage = false;
  public showEncryptSuccessMessage = false;
  public showDecryptErrorMessage = false;
  public showDecryptSuccessMessage = false;
  public showInvalidEncryptPasswordMessage = false;
  public showInvalidDecryptPasswordMessage = false;
  public selectedFile: File = null;
  public sharedUrl = "";
  public encryptPassword = "";
  public decryptPassword = "";
  public friends = new Array<User>();

  constructor(
    private apiService: APIService,
    private storageService: StorageService,
    private router: Router,
    aroute: ActivatedRoute) {
    aroute.url.subscribe((data) => {
      this.segments = data;
      console.log('segments ', this.segments);
    });
    this.segments.forEach(function (segment) {
      console.log(segment)
    });
  }

  ngOnInit() {
    this.getFiles();
  }

  public compare(a, b) {
    if (a.is_dir && !b.is_dir)
      return -1;
    if (!a.is_dir && b.is_dir)
      return 1;
    if (a.creation_date > b.creation_date)
      return -1;
    else
      return 1;
  }

  public getFiles() {
    if (!this.storageService.isAuthenticated()) {
      this.router.navigate(['/login']);
    }
    this.apiService.getFiles(this.router.url).subscribe(
      data => {
        this.files = data.sort(this.compare);
        this.getStats(this.files);
      },
      err => {
        this.router.navigate(['/login']);
      }
    );
  }

  public goToLowerDirectory(file) {
    this.refreshErrors();
    if (file.is_dir) {
      this.router.navigate(['files/' + file.path]).then(() => {
        this.getFiles();
      });
    }
  }

  public refreshErrors() {
    this.showDeleteErrorMessage = false;
    this.showAddFileErrorMessage = false;
    this.showAddDirErrorMessage = false;
    this.showInvalidFolderNameMessage = false;
    this.showEmptyFileMessage = false;
    this.showShareErrorMessage = false;
    this.showShareSuccessMessage = false;
    this.showEncryptErrorMessage = false;
    this.showEncryptSuccessMessage = false;
    this.showDecryptErrorMessage = false;
    this.showDecryptSuccessMessage = false;
    this.showInvalidEncryptPasswordMessage = false;
    this.showInvalidDecryptPasswordMessage = false;
  }

  public addFile() {
    this.refreshErrors();
    (<HTMLInputElement>document.getElementById('inputFile')).value = null;
    (<HTMLInputElement>document.getElementById('showAddFileModalButton')).click();
  }

  public confirmAddFile() {
    const selectedFile = (<HTMLInputElement>document.getElementById('inputFile')).files[0];
    console.log(selectedFile);
    if (!selectedFile) {
      this.showEmptyFileMessage = true;
      return;
    }
    document.getElementById('closeAddFileModalButton').click();
    this.apiService.uploadFile(this.router.url + "/", selectedFile).subscribe(
      data => this.getFiles(),
      err => this.showAddFileErrorMessage = true
    );
  }

  public createDirectory() {
    this.refreshErrors();
    (<HTMLInputElement>document.getElementById('dirName')).value = null;
    (<HTMLInputElement>document.getElementById('showAddDirModalButton')).click();
  }

  public confirmCreateDirectory() {
    if((<HTMLInputElement>document.getElementById('dirName')).value == "") {
      this.showInvalidFolderNameMessage = true;
      return;
    }
    document.getElementById('closeAddDirModalButton').click();
    console.log((<HTMLInputElement>document.getElementById('dirName')).value);
    this.apiService.uploadDir(this.router.url
      + "/" +(<HTMLInputElement>document.getElementById('dirName')).value).subscribe(
      data => this.getFiles(),
      err => this.showAddDirErrorMessage = true
    );
  }

  public download(file) {
    this.refreshErrors();
    this.apiService.download(file.path)
  }

  public delete(file) {
    this.refreshErrors();
    this.selectedFile = file;
    document.getElementById('showDeleteFileModalButton').click();
  }

  public confirmDeleteFile() {
    document.getElementById('closeDeleteFileModalButton').click();
    this.apiService.delete(this.selectedFile.path).subscribe(
      data => this.getFiles(),
      err => this.showDeleteErrorMessage = true
    );
  }

  public shareAsLink(file) {
    this.refreshErrors();
    this.apiService.share(file.path, true, null).subscribe(
      data => {
        console.log(data);
        this.sharedUrl = window.location.origin + '/share/' + (<SharedFile>data).id;
        console.log(this.sharedUrl)
        document.getElementById('showShareAsLinkModalButton').click();
      },
      err => this.showShareErrorMessage = true
    );
  }

  public shareWithFriends(file) {
    this.refreshErrors();
    this.selectedFile = file;
    this.apiService.getFriends().subscribe(
      data => this.friends = data,
      err => this.showShareErrorMessage = true
    );
    document.getElementById('showShareWithFriendsModalButton').click();
  }

  public confirmShareWithFriends() {
    let selectedFriends = Array.from((<HTMLInputElement>document.getElementById('selectedFriends'))
      .querySelectorAll("option:checked"),e => (<HTMLInputElement>e).value);
    document.getElementById('closeShareWithFriendsModalButton').click();
    this.apiService.share(this.selectedFile.path, false, selectedFriends).subscribe(
      data =>
        this.showShareSuccessMessage = true,
      err =>
        this.showShareErrorMessage = true
    )
  }

  public copyToClipBoard() {
    let element = <HTMLInputElement>document.getElementById('linkToSharedResource');
    element.select();
    document.execCommand('copy');
    element.setSelectionRange(0, 0);
  }

  public encrypt(file) {
    this.refreshErrors();
    this.selectedFile = file;
    this.encryptPassword = "";
    document.getElementById('showEncryptModalButton').click();
  }

  public confirmEncrypt() {
    if(this.encryptPassword == "") {
      this.showInvalidEncryptPasswordMessage = true;
      return;
    }
    document.getElementById('closeEncryptModalButton').click();
    this.apiService.encrypt(this.selectedFile.path, this.encryptPassword).subscribe(
      data => {
        this.showEncryptSuccessMessage = true;
        this.getFiles();
      },
      err => this.showEncryptErrorMessage = true
    )
  }

  public decrypt(file) {
    this.refreshErrors();
    this.selectedFile = file;
    this.decryptPassword = "";
    document.getElementById('showDecryptModalButton').click();
  }

  public confirmDecrypt() {
    if(this.decryptPassword == "") {
      this.showInvalidDecryptPasswordMessage = true;
      return;
    }
    document.getElementById('closeDecryptModalButton').click();
    this.apiService.decrypt(this.selectedFile.path, this.decryptPassword).subscribe(
      data => {
        this.showDecryptSuccessMessage = true;
        this.getFiles();
      },
      err => this.showDecryptErrorMessage = true
    );
  }

  public isEncrypted(file) {
    return file.name.includes('~encrypted');
  }

  public filesNotEmpty() {
    return this.files.length !== 0;
  }

  public goToUpperDirectory(segmentIndex) {
    this.refreshErrors();
    let path = "";
    for (let i = 0; i < segmentIndex; i++) {
      path += this.segments[i].path + "/"
    }
    this.router.navigate(['files/'+path]).then(()=>{
      this.getFiles();
    });
  }

  public getFileAsset(filename) {
    let extension = filename.split('.').pop();
    if (extension == "gitignore") extension = "git";
    else if (extension == "gitkeep") extension = "git";
    else if (extension == "docx") extension = "doc";
    else if (extension == "xlsx") extension = "xls";
    let list = ["zip", "xml", "xls", "txt", "svg", "sql", "rar", "png", "php", "pdf", "mp3", "md", "json", "js", "jpg", "ini", "html", "gz", "git", "gif", "exe", "doc", "dll", "default", "csv", "css", "bmp", "py"];
    if (list.includes(extension))
      return "assets/img/" + extension + ".png";
    else
      return "assets/img/default.png";
  }

  private getStats(files) {
    let cutPath = this.router.url.substring(1, this.router.url.length);
    cutPath = cutPath.substring(cutPath.indexOf('/')).substring(this.router.url.indexOf('/'), this.router.url.length);
    if (cutPath.indexOf('/') == -1) {
      cutPath = "";
    }
    this.apiService.getStats(cutPath).subscribe(
      data => {
        function onlyUnique(value, index, self) {
          return self.indexOf(value) === index;
        }
        let stats = new Array<Stat>();
        let filenames = data.map(obj => obj.filename);
            filenames = filenames.filter( onlyUnique );
            filenames.forEach(function (filename) {
            let sum = 0;
            data.filter(obj => obj.filename==filename).forEach(function (obj) {
              sum += +obj.count;
            });
            let stat = new Stat();
            stat.count = sum == -1 ? +'' : sum;
            stat.filename = filename;
            stats.push(stat)
          });
        this.stats = stats;
      },
      err => console.log(err)
    );
  }

  public getStat(filename) {
    if (this.stats.filter(obj => obj.filename == filename).length > 0) {
      return this.stats.filter(obj => obj.filename == filename).pop().count;
    }
    else {
      return '';
    }
  }
}
