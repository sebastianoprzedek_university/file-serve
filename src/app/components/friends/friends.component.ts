import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {User} from "../../models/user.model";
import {Router} from "@angular/router";
import {FormControl, FormGroup} from "@angular/forms";
import {Request} from "../../models/request.model";

@Component({
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {

  public users = new Array<User>();
  public requests = new Array<Request>();
  public friends = new Array<User>();
  public addFriendError = false;
  public addFriendSuccess = false;
  public acceptFriendSuccess = false;
  public acceptFriendError = false;
  public rejectFriendSuccess = false;
  public rejectFriendError = false;
  public deleteFriendError = false;
  public deleteFriendSuccess = false;

  constructor(private apiService: APIService,
              private router: Router) {
  }

  addFriendForm = new FormGroup ({
    username: new FormControl()
  });

  ngOnInit() {
    this.getUsers();
    this.getRequests();
    this.getFriends();
  }

  public getUsers() {
    this.apiService.getUsers().subscribe(
      data => this.users = data,
      err => console.error(err)
    );
  }

  public getFriends() {
    this.apiService.getFriends().subscribe(
      data => this.friends = data,
      err => this.router.navigate(['/login'])
    );
  }

  public getRequests() {
    this.apiService.getRequests().subscribe(
      data => this.requests = data,
      err => console.error(err)
    );
  }

  public refreshFriends() {
    this.getUsers();
    this.getRequests();
    this.getFriends();
  }

  public refreshMessages() {
    this.addFriendError = false;
    this.addFriendSuccess = false;
    this.deleteFriendError = false;
    this.deleteFriendSuccess = false;
    this.acceptFriendSuccess = false;
    this.acceptFriendError = false;
    this.rejectFriendSuccess = false;
    this.rejectFriendError = false;
  }

  public accept(id) {
    this.refreshMessages();
    this.apiService.acceptFriendRequest(id).subscribe(
      data => {
        this.acceptFriendSuccess = true
        this.refreshFriends();
      },
      err => this.acceptFriendError = true
    )
  }

  public reject(id) {
    this.refreshMessages();
    this.apiService.rejectFriendRequest(id).subscribe(
      data => {
        this.rejectFriendSuccess = true;
        this.refreshFriends();
      },
      err => this.rejectFriendError = true
    )
  }

  addFriend(event: Event) {
    event.preventDefault();
    let user = this.findUserByUsername(this.addFriendForm.controls['username'].value);
    if (!user) {
      this.addFriendError = true;
      return;
    }
    this.refreshMessages();
    console.log("addFriend");
    this.apiService.addFriend(user.id).subscribe(
      data => {
        this.addFriendSuccess = true;
        this.getFriends();
      },
        err => this.addFriendError = true
    );
  }

  public findUserByUsername(username: string) {
    return this.users.filter(friend => friend.username == username).pop();
  }

  public findUsernameById(id) {
    if (this.users.filter(friend => friend.id == id).length > 0)
      return this.users.filter(friend => friend.id == id).pop().username;
    else
      return "";
  }

  public remove(user) {
    this.refreshMessages();
    this.apiService.removeFriend(user.id).subscribe(
      data => {
        this.deleteFriendSuccess = true;
        this.getFriends();
      },
        err => this.deleteFriendError = true
    );
  }
}
