import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {APIService} from '../../services/api.service';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {StorageService} from "../../services/storage.service";

@Component({
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  public userDetails = {username: '', password: '', password2: '', mail: ''};
  public error = false;
  public errorMessage = "";

  constructor(private router: Router,
              private apiService: APIService,
              private formBuilder: FormBuilder,
              private storageService: StorageService) {
  }

  registerForm = new FormGroup ({
    username: new FormControl(),
    password: new FormControl(),
    password2: new FormControl(),
    mail: new FormControl(),
    regulations: new FormControl(),
  });

  register(event: Event) {
    event.preventDefault();
    this.userDetails.username = this.registerForm.controls['username'].value;
    this.userDetails.password = this.registerForm.controls['password'].value;
    this.userDetails.password2 = this.registerForm.controls['password2'].value;
    this.userDetails.mail = this.registerForm.controls['mail'].value;
    this.error = false;

    function mapToPolish(errorMessage: string) {
      let polishMessage = "";
      if (errorMessage.includes('A user with that username already exists.')) {
        polishMessage += 'Login jest już zajęty. ';
      }
      if (errorMessage.includes('Enter a valid username. This value may contain only letters, numbers, and @/./+/-/_ characters.')) {
        polishMessage += 'Login może zawierać tylko litery, cyfry oraz znaki specjalne @/./+/-/_. ';
      }
      if (errorMessage.includes('This password is too short. It must contain at least 8 characters.')) {
        polishMessage += 'Hasło powinno zawierać minimum 8 znaków. ';
      }
      if (errorMessage.includes('This password is entirely numeric')) {
        polishMessage += 'Hasło nie może zawierać samych cyfr. ';
      }
      if (errorMessage.includes('This password is too common.')) {
        polishMessage += 'Siła hasła jest zbyt słaba. ';
      }
      if (errorMessage.includes('Enter a valid email address.')) {
        polishMessage += 'Niepoprawny format adresu email. ';
      }
      return polishMessage;
    }

    this.apiService.register(this.userDetails).subscribe(data => {
          this.login(event);
      },
      err => {
        this.errorMessage = "";
        if (err.error.username) this.errorMessage += err.error.username;
        if (err.error.password1) this.errorMessage += err.error.password1;
        if (err.error.password2) this.errorMessage += err.error.password2;
        if (err.error.mail) this.errorMessage += err.error.mail;
        this.errorMessage = mapToPolish(this.errorMessage);
        this.error = true;
      });
  }

  login(event: Event) {
    event.preventDefault();
    const credentials = {username: this.userDetails.username, password: this.userDetails.password};
    this.apiService.login(credentials).subscribe(data => {
        this.storageService.login();
        this.storageService.setUsername(this.userDetails.username);
        this.router.navigate(['/files']);
      },
      err => {
        console.log(err);
        this.error = true;
      }
    );
  }

  submitDisabled() {
    return this.registerForm.controls['username'].invalid ||
            this.registerForm.controls['password'].invalid ||
            this.registerForm.controls['password2'].invalid ||
            this.registerForm.controls['mail'].invalid ||
            this.registerForm.controls['regulations'].invalid ||
            this.registerForm.controls['password'].value != this.registerForm.controls['password2'].value;
  }
}
