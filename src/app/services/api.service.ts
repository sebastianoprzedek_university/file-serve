import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {File} from '../models/file.model';
import {CookieService} from "ngx-cookie-service";
import {User} from "../models/user.model";
import {SharedFile} from "../models/shared-file.model";
import {Stat} from "../models/stat.model";
import {Request} from "../models/request.model";

@Injectable({
  providedIn: 'root'
})

export class APIService {
  API_URL = '/api/v1';

  constructor(private httpClient: HttpClient,
              private cookieService: CookieService) {
  }

  acceptFriendRequest (id) {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    const toOther = {'from_user': id};
    return this.httpClient.post(this.API_URL + '/users/acceptRequest/',toOther,{headers: headers});
  }

  addFriend (userId) {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    const toOther = {'to_user': userId};
    return this.httpClient.post(this.API_URL + '/users/send/',toOther,{headers: headers});
  }

  delete (url) {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    return this.httpClient.delete(this.API_URL + '/files' + url,{headers: headers});
  }

  download (url) {
    window.open(this.API_URL + '/files' + url);
  }

  downloadSharedResource (url) {
    window.open(this.API_URL + '/share' + url);
  }

  encrypt(path, password) {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    const body = {'password': password};
    return this.httpClient.post(this.API_URL + '/encrypt/' + path + '/',body,{headers: headers});
  }

  decrypt(path, password) {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    const body = {'password': password, 'decrypt' : 'decrypt'};
    return this.httpClient.post(this.API_URL + '/encrypt/' + path + '/',body,{headers: headers});
  }

  getFiles (url) {
    return this.httpClient.get<File[]>(this.API_URL + url);
  }

  getFriends () {
    return this.httpClient.get<User[]>(this.API_URL + '/users/friendsList/');
  }

  getRequests () {
    return this.httpClient.get<Request[]>(this.API_URL + '/users/unrejectedRequestList/')
  }

  getSharedByMeFiles () {
    return this.httpClient.get<SharedFile[]>(this.API_URL + '/share/my/');
  }

  getSharedFiles () {
    return this.httpClient.get<File[]>(this.API_URL + '/share/');
  }

  getSharedResource (url) {
    return this.httpClient.get(this.API_URL + url, {responseType: 'text'});
  }

  getStats (url) {
    if(url == "") {
      return this.httpClient.get<Stat[]>(this.API_URL + "/users/stats/");
    } else {
      return this.httpClient.get<Stat[]>(this.API_URL + "/users/stats/?dir=" + url);
    }
  }

  getUsers () {
    return this.httpClient.get<User[]>(this.API_URL + '/users/list/');
  }

  login (credentials) {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    const encodedCredentials = {'username': credentials.username, 'password': credentials.password}; //TODO: btoa(credentials.password) or sth
    return this.httpClient.post(this.API_URL + '/rest-auth/login/', encodedCredentials,{headers: headers});
  }

  logout () {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    return this.httpClient.post(this.API_URL + '/rest-auth/logout/', null,{headers: headers});
  }

  register (credentials) {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    const encodedCredentials = {'username': credentials.username, 'password1': credentials.password, //TODO: btoa(credentials.password) or sth
      'password2': credentials.password2, 'mail': credentials.mail};
    return this.httpClient.post(this.API_URL + '/rest-auth/registration/', encodedCredentials,{headers: headers});
  }

  rejectFriendRequest (id) {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    const toOther = {'from_user': id};
    return this.httpClient.post(this.API_URL + '/users/rejectRequest/',toOther,{headers: headers});
  }

  removeFriend (userId) {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    const toOther = {'from_user': userId};
    return this.httpClient.post(this.API_URL + '/users/removeFriend/',toOther,{headers: headers});
  }

  share (url, isPublic, friends) {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    let shareData = {};
    if (isPublic) {
      shareData = {'path': url, 'is_public': true};
    } else {
      shareData = {'path': url, 'is_public': false, 'shared_with': friends};
    }
    console.log(shareData);
    return this.httpClient.post(this.API_URL + '/share/', shareData,{headers: headers});
  }

  unshare (id) {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    return this.httpClient.delete(this.API_URL + '/share/' + id, {headers: headers});
  }

  uploadFile (url, file) {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    let input = new FormData();
    input.append('f', file);
    return this.httpClient.post(this.API_URL + url, input,{headers: headers});
  }

  uploadDir (url) {
    const headers = new HttpHeaders().set('X-CSRFTOKEN', this.cookieService.get("csrftoken"));
    let input = new FormData();
    return this.httpClient.post(this.API_URL + url, input,{headers: headers});
  }
}
