import {Injectable} from '@angular/core';

const AUTH_KEY = 'AuthStatus';
const USERNAME_KEY = 'AuthName';

@Injectable()
export class StorageService {

  constructor() {
  }

  clear() {
    window.sessionStorage.removeItem(USERNAME_KEY);
    window.sessionStorage.removeItem(AUTH_KEY);
    window.sessionStorage.clear();
  }

  public getUsername() {
    return sessionStorage.getItem(USERNAME_KEY);
  }

  public setUsername(username) {
    window.sessionStorage.removeItem(USERNAME_KEY);
    window.sessionStorage.setItem(USERNAME_KEY, username);
  }

  public isAuthenticated(): boolean {
    return sessionStorage.getItem(AUTH_KEY) === 'true';
  }

  public login() {
    window.sessionStorage.removeItem(AUTH_KEY);
    window.sessionStorage.setItem(AUTH_KEY, 'true');
  }
}
