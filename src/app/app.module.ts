import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FilesComponent} from "./components/files/files.component";
import {LoginComponent} from "./components/login/login.component";
import {RegisterComponent} from "./components/register/register.component";
import {CookieService} from "ngx-cookie-service";
import {RegulationsComponent} from "./components/regulations/regulations.component";
import {StorageService} from "./services/storage.service";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {SharedListComponent} from "./components/shared-list/shared-list.component";
import {FriendsComponent} from "./components/friends/friends.component";
import {SharedResourceComponent} from "./components/shared-resource/shared-resource.component";
import {MySharingsComponent} from "./components/my-sharings/my-sharings.component";

@NgModule({
  declarations: [
    AppComponent,
    FilesComponent,
    FriendsComponent,
    LoginComponent,
    MySharingsComponent,
    RegisterComponent,
    RegulationsComponent,
    SharedListComponent,
    SharedResourceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    NgbModule
  ],
  providers: [
    CookieService, StorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
