export class SharedFile {
  id: number;
  path: string;
  owner: string;
  is_public: boolean;
  shared_with: Array<string>
}
