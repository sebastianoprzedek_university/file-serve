export class File {
  creation_date: Date;
  is_dir: boolean;
  name: string;
  owner: string;
  path: string;
  size: number;
}
